import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MaturityComponent} from './pages/maturity/maturity.component';

const routes: Routes = [
  { path: '',
    redirectTo: 'maturity',
    pathMatch: 'full',
  },
  { path: 'maturity',
    pathMatch: 'full',
    component: MaturityComponent,
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

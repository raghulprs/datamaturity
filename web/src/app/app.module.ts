import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatPaginatorModule} from '@angular/material/paginator';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

import { NavbarComponent } from './widgets/navbar/navbar.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatExpansionModule, MatSortModule,  MatTableModule, MatStepperModule, MatProgressBarModule, MatSnackBarModule, MatInputModule, MatButtonModule, MatIconModule, MatToolbarModule} from '@angular/material'

import {MatMenuModule} from '@angular/material/menu';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { MaturityComponent } from './pages/maturity/maturity.component';
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    MaturityComponent
  ],
  entryComponents: []
  ,
  imports: [
    BrowserModule, HttpClientModule,
    AppRoutingModule, MatSidenavModule,
    MatPaginatorModule, MatExpansionModule, MatMenuModule,
    BrowserModule, MatSortModule, BrowserAnimationsModule, ReactiveFormsModule, FormsModule,
    AppRoutingModule, MatProgressBarModule, MatTableModule, MatStepperModule, MatSnackBarModule, MatInputModule, MatButtonModule, MatIconModule, MatToolbarModule
  ],
  providers: [HttpClientModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
